﻿using BioFingerprint.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioFingerprint
{
    public static class FingerprintComparer
    {
        public static double positionMargin;
        public static double angleMargin;
        public static int minimalMinutiaNumber;
        /// <summary>
        /// Compares two fingerprints, and returns true if they represent the same fingerprint
        /// </summary>
        /// <param name="minuteList_1"></param>
        /// <param name="minuteList_2"></param>
        /// <returns></returns>
        public static bool CompareFingerprints(List<Minute> minuteListOriginal_1, List<Minute> minuteListOriginal_2, double positionMargin, double angleMargin, int minimalMinutiaNumber)
        {
            List<Minute> minuteList_1 = new List<Minute>();
            List<Minute> minuteList_2 = new List<Minute>();
            foreach (var item in minuteListOriginal_1)
            {
                minuteList_1.Add(new Minute()
                {
                    Angle = item.Angle,
                    MinuteType = item.MinuteType,
                    PixelPoint = new System.Drawing.Point()
                    {
                        X = item.PixelPoint.X,
                        Y = item.PixelPoint.Y
                    }
                });
            }
            foreach (var item in minuteListOriginal_2)
            {
                minuteList_2.Add(new Minute()
                {
                    Angle = item.Angle,
                    MinuteType = item.MinuteType,
                    PixelPoint = new System.Drawing.Point()
                    {
                        X = item.PixelPoint.X,
                        Y = item.PixelPoint.Y
                    }
                });
            }


            FingerprintComparer.positionMargin = positionMargin;
            FingerprintComparer.angleMargin = angleMargin;
            FingerprintComparer.minimalMinutiaNumber = minimalMinutiaNumber;
            int correctMinutiaNumber = 0;
            for (int i = 0; i < minuteList_1.Count; i++)
            {
                for (int j = 0; j < minuteList_2.Count; j++)
                {
                    if (CompareMinutes(minuteList_1[i], minuteList_2[j]))
                    {
                        correctMinutiaNumber++;
                        minuteList_1.RemoveAt(i);
                        minuteList_2.RemoveAt(j);
                        i = 0;
                        j = 0;
                    }
                }
            }

            ((MainWindow)System.Windows.Application.Current.MainWindow).Stats_Correct.Text = correctMinutiaNumber.ToString();
           
            if (correctMinutiaNumber >= minimalMinutiaNumber)
                return true;

            else
                return false;
        }

        public static bool CompareMinutes(Minute minute_1, Minute minute_2)
        {
            // Euclidian distance
            System.Drawing.Point p1 = minute_1.PixelPoint;
            System.Drawing.Point p2 = minute_2.PixelPoint;
            double distance = Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));

            if (distance > positionMargin)
                return false;

            if (Math.Abs(minute_1.Angle - minute_2.Angle) > angleMargin)
                return false;

            if (minute_1.MinuteType != minute_2.MinuteType)
                return false;

            // otherwise
            return true;
        }





    }
}
