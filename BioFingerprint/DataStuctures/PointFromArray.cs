﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioFingerprint.DataStuctures
{
    public class PointFromArray
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
