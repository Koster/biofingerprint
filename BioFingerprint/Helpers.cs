﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BioFingerprint.Resources;
using Microsoft.Win32;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace BioFingerprint
{
    public static class Helpers
    {
        static int black_int = 0;
        static int white_int = 255;

        public static void DisableControls(List<Control> controlList)
        {
            foreach (Control ctr in controlList)
            {
                ctr.IsEnabled = false;
            }
        }

        public static void EnableControls(List<Control> controlList)
        {
            foreach (Control ctr in controlList)
            {
                ctr.IsEnabled = true;
            }
        }

        public static System.Drawing.Bitmap ChoseImageFromFile_ReturnGrayscaleBitmap(
            string title = "Select image",
            string filter = "All supported graphics|*.jpg;*.jpeg;*.png|"
            + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|"
            + "Portable Network Graphic (*.png)|*.png;")
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = title;
            op.Filter = filter;
            if (op.ShowDialog() == true)
            {

                System.Drawing.Bitmap loadedBitmap = ConvertBitmapImageToBitmap(new BitmapImage(new Uri(op.FileName)));
                return ImageTools.GrayscaleImage(loadedBitmap);
            }
            else
                return null;
        }

        public static void LoadBitmapToContainerInLayout(System.Windows.Controls.Image imageControl, System.Drawing.Bitmap bitmapToLoad)
        {
            imageControl.Source = ConvertBitmapToImageSource(bitmapToLoad);
        }


        public static void ClearImages(List<System.Windows.Controls.Image> controlList)
        {
            foreach (System.Windows.Controls.Image img in controlList)
            {
                img.Source = null;
            }
        }

        public static void SetPlotLineXValue(Line plotLine, double newValue)
        {
            plotLine.X1 = newValue;
            plotLine.X2 = newValue;
        }



        public static System.Drawing.Bitmap ConvertBitmapImageToBitmap(BitmapImage bitmapImage)
        {
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);
                return new System.Drawing.Bitmap(bitmap);
            }
        }

        public static BitmapImage ConvertBitmapToImageSource(System.Drawing.Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();
                return bitmapimage;
            }
        }


        #region convertion methods
        public static Bitmap ArrayToBitmap(int[,] array, Bitmap bmp, int width, int height)
        {
            BitmapData data = bmp.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            // Copy the bytes from the image into a byte array
            byte[] bytes = new byte[data.Height * data.Stride];
            Marshal.Copy(data.Scan0, bytes, 0, bytes.Length);

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    byte color;
                    if (array[i, j] == 1)
                        color = (byte)black_int;
                    else if (array[i, j] == 0)
                        color = (byte)white_int;
                    else
                        throw new Exception("Helpers -> ArrayToBitmap -> found pixel that is neither black nor white in binary image");
                    bytes[j * data.Stride + i] = color; // Set the pixel to color
                }
            }

            // Copy the bytes from the byte array into the image
            Marshal.Copy(bytes, 0, data.Scan0, bytes.Length);
            bmp.UnlockBits(data);
            return bmp;
        }

        public static int[,] BitmapToArray(Bitmap bmp, int[,] array, int width, int height)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    var dowywalenia = bmp.GetPixel(i, j);
                    int pixel = bmp.GetPixel(i, j).B;
                    if (pixel == black_int)
                        array[i, j] = 1;
                    else if (pixel == white_int)
                    {
                        array[i, j] = 0;
                    }
                    else
                        throw new Exception("Helpers -> BitmapToArray -> found pixel that is neither black nor white in binary image");
                }
            }
            return array;
        }
        #endregion



    }
}
