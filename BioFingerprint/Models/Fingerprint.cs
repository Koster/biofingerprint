﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioFingerprint.Models
{
    public class Fingerprint
    {
        public string Identifier { get; set; }
        public List<Minute> MinuteList { get; set; }
    }
}
