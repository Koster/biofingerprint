﻿using BioFingerprint.Resources.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BioFingerprint.Models
{
    public class Database
    {
        private string nameFile = "Database.xml";
        private int actualId = 0;
        private static Database database;
        private Database()
        {
            if(FileExist() == false)
            {
                AddDatabaseFile();
            }
            else
            {
                XmlDocument doc = XMLReader();
                XmlNode elem = doc.LastChild;
                if(elem.ChildNodes.Count != 0)
                    actualId = int.Parse(elem.LastChild.FirstChild.InnerText);
            }
        }

        public int GenerateNewId()
        {
            actualId++;
            return actualId;
        }

        public static Database getInstance()
        {
            if (database == null) database = new Database();
            return database;
        }

        private bool FileExist()
        {
            DirectoryInfo dir = new DirectoryInfo(Directory.GetCurrentDirectory());
            foreach (var file in dir.GetFiles())
            {
                if (file.Name.Equals(nameFile)) return true;

            }
            return false;
        }

        private void AddDatabaseFile()
        {
            XmlDocument doc = new XmlDocument();
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "yes");
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);
            XmlElement elem = doc.CreateElement("Accounts");
            doc.AppendChild(elem);
            doc.Save(nameFile);
        }

        private XmlDocument XMLReader()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(nameFile);
            return doc;
        }
        
        public void AddAccount(string accountName, int newId, List<Minute> minutes)
        {
            XmlDocument doc = XMLReader();
            XmlElement account = doc.CreateElement("Account");
            XmlElement id = doc.CreateElement("Id");
            XmlElement name = doc.CreateElement("Name");
            XmlElement allMinutes = doc.CreateElement("AllMinutes");
            account.AppendChild(id);
            account.AppendChild(name);
            account.AppendChild(allMinutes);
            id.InnerText = newId.ToString();
            name.InnerText = accountName;


            foreach (var minute in minutes)
            {
                XmlNode minuteList = doc.CreateElement("minuteList");
                XmlNode x = doc.CreateElement("x");
                XmlNode y = doc.CreateElement("y");
                XmlNode angle = doc.CreateElement("Angle");
                XmlNode minuteType = doc.CreateElement("minuteType");
                XmlNode distanceFromCenter = doc.CreateElement("DistanceFromCenter");
                minuteList.AppendChild(x);
                minuteList.AppendChild(y);
                minuteList.AppendChild(angle);
                minuteList.AppendChild(distanceFromCenter);
                minuteList.AppendChild(minuteType);
                allMinutes.AppendChild(minuteList);
                x.InnerText = minute.PixelPoint.X.ToString();
                y.InnerText = minute.PixelPoint.Y.ToString();
                angle.InnerText = minute.Angle.ToString();
                minuteType.InnerText = ((int)minute.MinuteType).ToString();
            }
                        
            doc.LastChild.AppendChild(account);
            doc.Save(nameFile);
        }

        public ObservableCollection<Account> GetAccounts()
        {
            ObservableCollection<Account> accounts = new ObservableCollection<Account>();
            XmlDocument doc = XMLReader();
            foreach(XmlNode item in doc.LastChild.ChildNodes)
            {
                var newMinuteList = new List<Minute>();

                foreach (XmlNode minuteItem in item.ChildNodes[2].ChildNodes)
                {
                    newMinuteList.Add(new Minute()
                    {
                        PixelPoint = new System.Drawing.Point()
                        {
                             X=Convert.ToInt32(minuteItem.ChildNodes[0].InnerText),
                             Y = Convert.ToInt32(minuteItem.ChildNodes[1].InnerText),
                        },
                        Angle = Convert.ToInt32(minuteItem.ChildNodes[2].InnerText),
                        MinuteType= (Enums.MinutiaeTypeEnum)Enum.Parse(typeof(Enums.MinutiaeTypeEnum), minuteItem.ChildNodes[4].InnerText)
                });
                }

                accounts.Add(new Account()
                {
                    id = int.Parse(item.ChildNodes[0].InnerText),
                    name = item.ChildNodes[1].InnerText,
                    minuteList = newMinuteList
                });

            }
            return accounts;
        }

        public void RemoveAccount(int id)
        {
            XmlDocument doc = XMLReader();
            XmlNodeList accounts = doc.LastChild.ChildNodes;
            for (int i = 0; i < accounts.Count; i++)
            {
                if(int.Parse(accounts[i].FirstChild.InnerText) == id)
                {
                    accounts[i].ParentNode.RemoveChild(accounts[i]);
                    break;
                }
            }
            doc.Save(nameFile);
        }
    }
}
