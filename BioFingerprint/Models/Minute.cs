﻿using BioFingerprint.Resources.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioFingerprint.Models
{
    public class Minute
    {
        public Enums.MinutiaeTypeEnum MinuteType { get; set; }
        public Point  PixelPoint { get; set; }
        public int Angle { get; set; }

    }
}
