﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioFingerprint.Models
{
    public class Account
    {
        public int id { get; set;}
        public string name { get; set; }
        public List<Minute> minuteList { get; set; }
        //public string x { get; set; }
        //public string y { get; set; }
        //public string angle { get; set; }
        //public string distanceFromCenter { get; set; }
    }
}
