﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BioFingerprint.Resources;
using BioFingerprint.Resources.Enums;
using Microsoft.Win32;
using BioFingerprint.Models;
using System.Collections.ObjectModel;

namespace BioFingerprint
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields and properties
        public Enums.TestTypeEnum TestType;
        private bool SlidersLock = false;
        private bool areSlidersInitiated = false;
        FingerprintImages fingerprintImages = new FingerprintImages();
        private int numberOfFingerScan = 1;
        ObservableCollection<Account> accounts = new ObservableCollection<Account>();
        List<BitmapImage> scans = new List<BitmapImage>();
        Fingerprint CurrentlyLoadedFingerprint = new Fingerprint();
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            InitGeneral();
        }

        private void InitGeneral()
        {
            TestType = Enums.TestTypeEnum.None;
            InitRatePlot();
            areSlidersInitiated = true;
            AccountsList.ItemsSource = Database.getInstance().GetAccounts();
        }

        public void InitRatePlot()
        {
            try
            {
                ImageBrush ib = new ImageBrush();
                ib.ImageSource = new BitmapImage(new Uri(@URIs.UriDictionay.First(el => el.Key == "ratePlotUri").Value, UriKind.Relative));
                RatePlotBackgournd.Background = ib;
            }
            catch (Exception e)
            {
                MessageBoxResult result = MessageBox.Show(e.Message);
                if (result == MessageBoxResult.Yes)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        private void LoadFingerPrint_ButtonClick_Click(object sender, RoutedEventArgs e)
        {
            fingerprintImages.sourceFingerprint = Helpers.ChoseImageFromFile_ReturnGrayscaleBitmap();
            if (fingerprintImages.sourceFingerprint != null)
            {

                //Helpers.LoadBitmapToContainerInLayout(InputFingerprint_Image, fingerprintImages.sourceFingerprint);

                fingerprintImages.enhancedSourceFingerprint = ImageTools.NormalizeImage(fingerprintImages.sourceFingerprint);
                fingerprintImages.enhancedSourceFingerprint = ImageTools.Median(fingerprintImages.enhancedSourceFingerprint);
                fingerprintImages.enhancedSourceFingerprint = ImageTools.BradleyLocalThresholding(fingerprintImages.enhancedSourceFingerprint);
                fingerprintImages.enhancedSourceFingerprint_2 = K3M_Thinning.ThinImage(fingerprintImages.enhancedSourceFingerprint);

                CurrentlyLoadedFingerprint.MinuteList = MinuteExtractor.GetMinutesFromThinnedImage(fingerprintImages.enhancedSourceFingerprint_2, fingerprintImages);
                Helpers.LoadBitmapToContainerInLayout(InputFingerprintWithMinutes_Image, fingerprintImages.minutiaFingerprint);

                
            }


        }

        private void TestTypeRadioGroup_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton radioButton = (RadioButton)e.OriginalSource;
            if (radioButton.Name == "Identification")
            {
                InitIdentification();
            }
            else if (radioButton.Name == "Verification")
            {
                InitVerification();
            }
            else
                throw new Exception("Exception in 'check' event - choosing test type");

            LoadFingerPrint_Button.IsEnabled = true;
        }

        public void InitAndClearAfterChangingTestType()
        {
            Helpers.DisableControls(new List<Control> { RunTest_Button, FingerprintIdentificator_TextBox });
            Helpers.ClearImages(new List<Image> { InputFingerprintWithMinutes_Image });
            FingerprintIdentificator_TextBox.ClearValue(TextBox.TextProperty);
        }

        public void InitIdentification()
        {
            InitAndClearAfterChangingTestType();
            this.TestType = Enums.TestTypeEnum.Identification;
            Helpers.EnableControls(new List<Control> { RunTest_Button });
        }

        public void InitVerification()
        {
            InitAndClearAfterChangingTestType();
            this.TestType = Enums.TestTypeEnum.Verification;
            Helpers.EnableControls(new List<Control> { FingerprintIdentificator_TextBox, RunTest_Button });
        }

        private void RunTest_Button_Click(object sender, RoutedEventArgs e)
        {
            List<Account> accounts = Database.getInstance().GetAccounts().ToList();
            bool isLegit = false;
            Dictionary<string, int> usersCollection = new Dictionary<string, int>();

            int distanceMargin = 10;
            int angleMargin = 0;
            int minimalNrOfMinutes = 40;
            Stats_DistanceMargin.Text = distanceMargin.ToString();
            Stats_AngleMargin.Text = angleMargin.ToString();
            Stats_MinimalMinutes.Text = minimalNrOfMinutes.ToString();
            Stats_AllPoints.Text = CurrentlyLoadedFingerprint.MinuteList.Count.ToString();
            foreach (var account in accounts)
            {
                if (FingerprintComparer.CompareFingerprints(account.minuteList, CurrentlyLoadedFingerprint.MinuteList, distanceMargin, angleMargin, minimalNrOfMinutes) == true)
                {
                    isLegit = true;
                    usersCollection.Add(account.name, account.id);
                }
            }

            if (usersCollection.Count > 1)
            {
                MessageBox.Show("MULTIPLE IDENTIFIED USER DETECTED!");
            }

            if (isLegit == true)
            {
                Result_TextBlock.Text = "Identified: '"+usersCollection.First().Key+"'";
                Result_TextBlock.Background = Brushes.Green;
            }
            else
            {
                Result_TextBlock.Text = "Identification failed";
                Result_TextBlock.Background = Brushes.IndianRed;
            }
        }

        // Line min: 20, max: 715
        private void Plot_Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (areSlidersInitiated)
            {
                if (!SlidersLock)
                {
                    Slider targetSlider = (Slider)e.OriginalSource;
                    Slider anotherSlider;
                    if (targetSlider == FAR_Slider)
                    {
                        anotherSlider = FRR_Slider;
                    }
                    else if (targetSlider == FRR_Slider)
                    {
                        anotherSlider = FAR_Slider;
                    }
                    else
                        throw new Exception("Use of unexpected slider");
                    anotherSlider.Value = anotherSlider.Maximum - targetSlider.Value;
                    SlidersLock = true;
                    Helpers.SetPlotLineXValue(PlotLine, FAR_Slider.Value + 20);
                }
                else
                {
                    SlidersLock = false;
                }
            }
        }



        private void MinFAR_ButtonClick(object sender, RoutedEventArgs e)
        {
            FAR_Slider.Value = FAR_Slider.Minimum;
            FRR_Slider.Value = FRR_Slider.Maximum;
            Helpers.SetPlotLineXValue(PlotLine, FAR_Slider.Value + 20);
        }

        private void EER_ButtonClick(object sender, RoutedEventArgs e)
        {
            FAR_Slider.Value = FAR_Slider.Maximum / 2;
            FRR_Slider.Value = FRR_Slider.Maximum / 2;
            Helpers.SetPlotLineXValue(PlotLine, FAR_Slider.Value + 20);
        }

        private void MinFRR_ButtonClick(object sender, RoutedEventArgs e)
        {
            FAR_Slider.Value = FAR_Slider.Maximum;
            FRR_Slider.Value = FRR_Slider.Minimum;
            Helpers.SetPlotLineXValue(PlotLine, FAR_Slider.Value + 20);
        }

        private void AddFingerScan(object sender, RoutedEventArgs e)
        {
            string fileName = "";
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = ".png";
            ofd.Filter = "Image (.png)|*.png|Image (.jpg)|*.jpg|Image (.gif)|*.gif";
            Nullable<bool> result = ofd.ShowDialog();
            if (result == true)
            {
                fileName = ofd.FileName;

                BitmapImage fingerScan = new BitmapImage(new Uri(fileName));
                scans.Add(fingerScan);
                image.Source = fingerScan;
                description.Children.Add(new TextBlock() { Margin = new Thickness(12), Text = "Finger Scan " + numberOfFingerScan });
                Button button = new Button() { Margin = new Thickness(10), Name = "fingerScan" + numberOfFingerScan, Content = "Show scan" };
                button.Click += ShowScan;
                data.Children.Add(button);
                numberOfFingerScan++;
            }
        }
        private void SaveAccount(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(name.Text)) return;
            int accountId = Database.getInstance().GenerateNewId();
           
            numberOfFingerScan = 1;
            if (!Directory.Exists("FingerScan")) Directory.CreateDirectory("FingerScan");
            foreach (BitmapImage item in scans)
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(item));
                using (var filestream = new FileStream("FingerScan/" + accountId + "_" + numberOfFingerScan + ".png", FileMode.Create))
                    encoder.Save(filestream);
                numberOfFingerScan++;
            }

            // extract minutia and add to list
            List<Minute> listOfScansMinutes = new List<Minute>();
            foreach (BitmapImage item in scans)
            {
                System.Drawing.Bitmap scanBitmap = Helpers.ConvertBitmapImageToBitmap(item);      
                scanBitmap = ImageTools.GrayscaleImage(scanBitmap);
                scanBitmap = ImageTools.NormalizeImage(scanBitmap);
                scanBitmap = ImageTools.Median(scanBitmap);
                scanBitmap = ImageTools.BradleyLocalThresholding(scanBitmap);
                scanBitmap = K3M_Thinning.ThinImage(scanBitmap);
                listOfScansMinutes.AddRange(MinuteExtractor.GetMinutesFromThinnedImage(scanBitmap, fingerprintImages));
            }

                // save to db and refresh
             Database.getInstance().AddAccount(name.Text, accountId, listOfScansMinutes);
            numberOfFingerScan = 1;
            AccountsList.ItemsSource = Database.getInstance().GetAccounts();
            for (int i = description.Children.Count - 1; i >= 1; i--)
            {
                description.Children.RemoveAt(i);
                data.Children.RemoveAt(i);
            }
            name.Text = "";
            image.Source = null;
        }

        private void RemoveAccount(object sender, RoutedEventArgs e)
        {
            Account account = ((sender as Button).DataContext as Account);
            Database.getInstance().RemoveAccount(account.id);
            AccountsList.ItemsSource = Database.getInstance().GetAccounts();
            DirectoryInfo directory = new DirectoryInfo("FingerScan");
            foreach (var item in directory.GetFiles())
            {
                if(int.Parse(item.Name.Split('_')[0]) == account.id)
                {
                    item.Delete();
                }
            }

        }

        private void ShowScan(object sender, RoutedEventArgs e)
        {
            string name = ((Button)sender).Name;
            name = name.Substring(10);
            image.Source = scans[int.Parse(name) - 1];
        }
    }
}
