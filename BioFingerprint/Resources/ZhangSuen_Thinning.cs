﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BioFingerprint.DataStuctures;

namespace BioFingerprint.Resources
{
    public static class ZhangSuen_Thinning
    {

        private static int width;
        private static int height;
        private static int[,] pixelsArray;
        private static int white_int = 255;
        private static int black_int = 0;


        public static Bitmap ThinImage(Bitmap image)
        {
            width = image.Width;
            height = image.Height;
            pixelsArray = new int[image.Width, image.Height];
            pixelsArray = Helpers.BitmapToArray(image, pixelsArray,width,height);

            int transationNumber = 0;
            int blackNeighbours = 0;
            List<PointFromArray> pointsToBeTurnedToWhite = new List<PointFromArray>();
            bool wasSometingChanged = true;

            pixelsArray = ClearArrayBorders(pixelsArray);

            while (wasSometingChanged)
            {
                wasSometingChanged = false;
                //Step one
                for (int i = 1; i < width - 1; i++)
                {
                    for (int j = 1; j < height - 1; j++)
                    {
                        if (IsPixelBlack(i, j))
                        {
                            transationNumber = HowManyTransitionsFromWhiteToBlack(i, j);
                            blackNeighbours = HowManyBlacks(i, j);
                            if ((blackNeighbours >= 2 && blackNeighbours <= 6) && transationNumber == 1)
                            {
                                if (pixelsArray[i, j - 1] == 0 || pixelsArray[i + 1, j] == 0 || pixelsArray[i, j + 1] == 0)
                                {
                                    if (pixelsArray[i + 1, j] == 0 || pixelsArray[i, j + 1] == 0 || pixelsArray[i - 1, j] == 0)
                                    {
                                        pointsToBeTurnedToWhite.Add(new PointFromArray() { X = i, Y = j });
                                    }
                                }
                            }
                        }
                    }
                }
                if (pointsToBeTurnedToWhite.Count > 0)
                    wasSometingChanged = true;
                pixelsArray = SetNotedPixelsToWhite(pixelsArray, pointsToBeTurnedToWhite);

                //Step two
                for (int i = 1; i < width - 1; i++)
                {
                    for (int j = 1; j < height - 1; j++)
                    {
                        if (IsPixelBlack(i, j))
                        {
                            transationNumber = HowManyTransitionsFromWhiteToBlack(i, j);
                            blackNeighbours = HowManyBlacks(i, j);
                            if ((blackNeighbours >= 2 && blackNeighbours <= 6) && transationNumber == 1)
                            {
                                if (pixelsArray[i, j - 1] == 0 || pixelsArray[i + 1, j] == 0 || pixelsArray[i - 1, j] == 0)
                                {
                                    if (pixelsArray[i, j - 1] == 0 || pixelsArray[i, j + 1] == 0 || pixelsArray[i - 1, j] == 0)
                                    {
                                        pointsToBeTurnedToWhite.Add(new PointFromArray() { X = i, Y = j });
                                    }
                                }
                            }
                        }
                    }
                }

                if (pointsToBeTurnedToWhite.Count > 0)
                    wasSometingChanged = true;
                pixelsArray = SetNotedPixelsToWhite(pixelsArray, pointsToBeTurnedToWhite);
            }
            //pixelsArray = InvertPixelArray(pixelsArray);
            return Helpers.ArrayToBitmap(pixelsArray, image,width,height);
        }

        private static int[,] SetNotedPixelsToWhite(int[,] array, List<PointFromArray> points)
        {
            foreach (PointFromArray point in points)
            {
                array[point.X, point.Y] = 0;
            }
            points.Clear();
            return array;
        }

        private static int[,] InvertPixelArray(int[,] array)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (array[i, j] == 1)
                        array[i, j] = 0;
                    else
                        array[i, j] = 1;
                }
            }
            return array;
        }

        private static bool IsPixelBlack(int x, int y)
        {
            if (pixelsArray[x, y] == 1)
                return true;
            else
                return false;
        }

        private static int HowManyTransitionsFromWhiteToBlack(int x, int y)
        {
            int result = 0;
            if (pixelsArray[x, y] == 0 && pixelsArray[x, y - 1] == 1)
                result++;
            if (pixelsArray[x, y - 1] == 0 && pixelsArray[x + 1, y - 1] == 1)
                result++;
            if (pixelsArray[x + 1, y - 1] == 0 && pixelsArray[x + 1, y] == 1)
                result++;
            if (pixelsArray[x + 1, y] == 0 && pixelsArray[x + 1, y + 1] == 1)
                result++;
            if (pixelsArray[x + 1, y + 1] == 0 && pixelsArray[x, y + 1] == 1)
                result++;
            if (pixelsArray[x, y + 1] == 0 && pixelsArray[x - 1, y + 1] == 1)
                result++;
            if (pixelsArray[x - 1, y + 1] == 0 && pixelsArray[x - 1, y] == 1)
                result++;
            if (pixelsArray[x - 1, y] == 0 && pixelsArray[x - 1, y - 1] == 1)
                result++;
            if (pixelsArray[x - 1, y - 1] == 0 && pixelsArray[x, y - 1] == 1)
                result++;
            return result;
        }

        private static int HowManyBlacks(int x, int y)
        {
            int result = 0;
            if (pixelsArray[x, y - 1] == 1)
                result++;
            if (pixelsArray[x + 1, y - 1] == 1)
                result++;
            if (pixelsArray[x + 1, y] == 1)
                result++;
            if (pixelsArray[x + 1, y + 1] == 1)
                result++;
            if (pixelsArray[x, y + 1] == 1)
                result++;
            if (pixelsArray[x - 1, y + 1] == 1)
                result++;
            if (pixelsArray[x - 1, y] == 1)
                result++;
            if (pixelsArray[x - 1, y - 1] == 1)
                result++;
            return result;
        }

        private static int[,] ClearArrayBorders(int[,] array)
        {
            for (int i = 0; i < width; i++)
            {
                array[i, 0] = 0;
                array[i, height-1] = 0;
            }
            for (int j = 0; j < height; j++)
            {
                array[0, j] = 0;
                array[width-1, j] = 0;
            }
            return array;
        }

    }
}
