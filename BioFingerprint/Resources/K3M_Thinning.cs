﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BioFingerprint.DataStuctures;

namespace BioFingerprint.Resources
{
    public static class K3M_Thinning
    {


        #region lookup arrays
        private static int[] A0 = new int[48]{
            3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56, 60,
            62, 63, 96, 112, 120, 124, 126, 127, 129, 131, 135,
            143, 159, 191, 192, 193, 195, 199, 207, 223, 224,
            225, 227, 231, 239, 240, 241, 243, 247, 248, 249,
            251, 252, 253, 254};
        private static int[] A1 = new int[8] { 7, 14, 28, 56, 112, 131, 193, 224 };
        private static int[] A2 = new int[16] {
            7, 14, 15, 28, 30, 56, 60, 112, 120, 131, 135,
            193, 195, 224, 225, 240};
        private static int[] A3 = new int[24] {
            7, 14, 15, 28, 30, 31, 56, 60, 62, 112, 120,
            124, 131, 135, 143, 193, 195, 199, 224, 225, 227,
            240, 241, 248};
        private static int[] A4 = new int[32] {
            7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120,
            124, 126, 131, 135, 143, 159, 193, 195, 199, 207,
            224, 225, 227, 231, 240, 241, 243, 248, 249, 252};
        private static int[] A5 = new int[36] {
            7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120,
            124, 126, 131, 135, 143, 159, 191, 193, 195, 199,
            207, 224, 225, 227, 231, 239, 240, 241, 243, 248,
            249, 251, 252, 254};
        private static int[] A1pix = new int[48] {
            3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56,
            60, 62, 63, 96, 112, 120, 124, 126, 127, 129, 131,
            135, 143, 159, 191, 192, 193, 195, 199, 207, 223,
            224, 225, 227, 231, 239, 240, 241, 243, 247, 248,
            249, 251, 252, 253, 254};
        private static int[,] mask = new int[3, 3] { { 128, 1, 2 }, { 64, 0, 4 }, { 32, 16, 8 } };

        #endregion

        private static int width;
        private static int height;
        private static int[,] pixelsArray;
        private static int white_int = 255;
        private static int black_int = 0;
        private static List<Point> borderPixels;





        public static Bitmap ThinImage(Bitmap image)
        {
            width = image.Width;
            height = image.Height;
            borderPixels = new List<Point>();
            bool wasSometingChanged = true;

            pixelsArray = new int[image.Width, image.Height];
            pixelsArray = Helpers.BitmapToArray(image, pixelsArray, width, height);

            pixelsArray = ClearArrayBorders(pixelsArray);

            while (wasSometingChanged)
            {
                wasSometingChanged = false;
                borderPixels.Clear(); /* Not sure czy to powinno tu być */

                // phase 0
                for (int i = 1; i < width - 1; i++)
                {
                    for (int j = 1; j < height - 1; j++)
                    {
                        int weight = CalculateWeight(i, j);
                        if (A0.Contains(weight))
                        {
                            borderPixels.Add(new Point(i, j));
                        }
                    }
                }


                // phase 1
                foreach (Point point in borderPixels)
                {
                    int weight = CalculateWeight(point.X, point.Y);
                    if (A1.Contains(weight) && pixelsArray[point.X, point.Y] != 0)
                    {
                        pixelsArray[point.X, point.Y] = 0;
                        wasSometingChanged = true;
                    }
                }

                // phase 2
                foreach (Point point in borderPixels)
                {
                    int weight = CalculateWeight(point.X, point.Y);
                    if (A2.Contains(weight) && pixelsArray[point.X, point.Y] != 0)
                    {
                        pixelsArray[point.X, point.Y] = 0;
                        wasSometingChanged = true;
                    }
                }

                // phase 3
                foreach (Point point in borderPixels)
                {
                    int weight = CalculateWeight(point.X, point.Y);
                    if (A3.Contains(weight) && pixelsArray[point.X, point.Y] != 0)
                    {
                        pixelsArray[point.X, point.Y] = 0;
                        wasSometingChanged = true;
                    }
                }


                // phase 4
                foreach (Point point in borderPixels)
                {
                    int weight = CalculateWeight(point.X, point.Y);
                    if (A4.Contains(weight) && pixelsArray[point.X, point.Y] != 0)
                    {
                        pixelsArray[point.X, point.Y] = 0;
                        wasSometingChanged = true;
                    }
                }

                // phase 5
                foreach (Point point in borderPixels)
                {
                    int weight = CalculateWeight(point.X, point.Y);
                    if (A5.Contains(weight) && pixelsArray[point.X, point.Y] != 0)
                    {
                        pixelsArray[point.X, point.Y] = 0;
                        wasSometingChanged = true;
                    }
                }

                // phase 6
                borderPixels.Clear();
            }

            // last phase - thinning to 1 piksel
            for (int i = 1; i < width - 1; i++)
            {
                for (int j = 1; j < height - 1; j++)
                {
                    int weight = CalculateWeight(i, j);
                    if (A0.Contains(weight))
                    {
                        pixelsArray[i, j] = 0;
                    }
                }
            }

            //pixelsArray = InvertBinaryImage(pixelsArray);
            return Helpers.ArrayToBitmap(pixelsArray, image,width,height);
        }



        private static int CalculateWeight(int x, int y)
        {
            int result = 0;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    result += mask[i + 1, j + 1] * pixelsArray[x + i, y + j];

                }
            }
            return result;
        }


        private static int[,] InvertBinaryImage(int[,] array)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (array[i, j] == 1)
                        array[i, j] = 0;
                    else
                        array[i, j] = 1;
                }
            }
            return array;
        }

        private static int[,] ClearArrayBorders(int[,] array)
        {
            for (int i = 0; i < width; i++)
            {
                array[i, 0] = 0;
                array[i, height - 1] = 0;
            }
            for (int j = 0; j < height; j++)
            {
                array[0, j] = 0;
                array[width - 1, j] = 0;
            }
            return array;
        }

    }
}
