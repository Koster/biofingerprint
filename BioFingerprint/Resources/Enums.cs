﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioFingerprint.Resources.Enums
{
    public class Enums
    {
       public enum TestTypeEnum
        {
            None,
            Identification = 1,
            Verification = 2
        };

        public enum MinutiaeTypeEnum
        {
            None,
            EndPoint = 1,
            BifurcationPoint = 2
        };

    }
}
