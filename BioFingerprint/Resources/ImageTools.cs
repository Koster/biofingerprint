﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Accord;
using Accord.Imaging;
using AForge.Imaging.Filters;
using AForge.Imaging.Formats;
using Accord.Imaging.Filters;

namespace BioFingerprint.Resources
{
    public class ImageTools
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        public static Bitmap GetEnhancedFingerprint(Bitmap sourceBitmapImage)
        {
            Bitmap bitmap = NormalizeImage(sourceBitmapImage);
            return bitmap;
        }

        public static Bitmap NormalizeImage(Bitmap sourceImage)
        {
            AForge.Imaging.Filters.ContrastStretch filter = new ContrastStretch();
            sourceImage = filter.Apply(sourceImage);
            return sourceImage;
        }

        public static Bitmap GrayscaleImage(Bitmap sourceImage)
        {
            //coefficients defined by ITU-R BT.709 ITU-R BT.709
            Grayscale filter = new Grayscale(0.2125, 0.7154, 0.0721);
            sourceImage = filter.Apply(sourceImage);
            return sourceImage;
        }

        public static Bitmap Binarization_Threshold(Bitmap sourceImage, int threshold)
        {
            // var tmp = sourceImage.PixelFormat;
            Threshold filter = new Threshold((int)threshold);
            sourceImage = filter.Apply(sourceImage);
            return sourceImage;
        }

        public static Bitmap Binarization_SISThreshold(Bitmap sourceImage, bool isathresholdAutomatic)
        {
            SISThreshold filter = new SISThreshold();
            sourceImage = filter.Apply(sourceImage);
            return sourceImage;
        }

        /// <summary>
        /// Method described by Derek Bradley and Gerhard Roth in the "Adaptive Thresholding Using the Integral Image" paper
        /// </summary>
        /// <param name="sourceImage"></param>
        /// <param name="isathresholdAutomatic"></param>
        /// <param name="threshold"></param>
        /// <returns></returns>
        public static Bitmap BradleyLocalThresholding(Bitmap sourceImage)
        {
            // brightness default threshold value is 0.15 ( 15% )
            // window default size is 41 ( must be odd! )
            BradleyLocalThresholding filter = new BradleyLocalThresholding();
            filter.WindowSize = 16;
            sourceImage = filter.Apply(sourceImage);
            return sourceImage;
        }

        public static Bitmap Thinning(Bitmap sourceImage)
        {
            ZhangSuen_Thinning.ThinImage(sourceImage);
            return sourceImage;
        }

        public static Bitmap Median(Bitmap sourceImage)
        {
            Median filter = new Median();
            sourceImage = filter.Apply(sourceImage);
            return sourceImage;
        }

        //public static Bitmap GaborFilter(Bitmap sourceImage)
        //{
        //    GaborFilter filter = new GaborFilter();
        //    sourceImage = filter.Apply(sourceImage);
        //    return sourceImage;
        //}

        //public static Bitmap Skeletonization(Bitmap sourceImage)
        //{
        //    SimpleSkeletonization filter = new SimpleSkeletonization(255, 0);
        //    sourceImage = filter.Apply(sourceImage);
        //    return sourceImage;
        //}

        //public static Bitmap Closing(Bitmap sourceImage)
        //{
        //    Closing filter = new Closing();
        //    sourceImage = filter.Apply(sourceImage);
        //    return sourceImage;
        //}


        //public static Bitmap BinaryDilatation3x3(Bitmap sourceImage)
        //{
        //    BinaryDilatation3x3 filter = new BinaryDilatation3x3();
        //    sourceImage = filter.Apply(sourceImage);
        //    return sourceImage;
        //}

        //public static Bitmap BinaryErosion3x3(Bitmap sourceImage)
        //{
        //    BinaryErosion3x3 filter = new BinaryErosion3x3();
        //    sourceImage = filter.Apply(sourceImage);
        //    return sourceImage;
        //}

    }
}
