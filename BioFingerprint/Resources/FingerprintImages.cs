﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioFingerprint.Resources
{
    public class FingerprintImages
    {
        public Bitmap sourceFingerprint { get; set; }
        public Bitmap enhancedSourceFingerprint { get; set; }
        public Bitmap enhancedSourceFingerprint_2 { get; set; }
        public Bitmap minutiaFingerprint { get; set; }
        public Dictionary<string, Bitmap> FingerprintImagesDictionay = new Dictionary<string, Bitmap>()
        {
        //  {"ratePlotUri", new Bitmap("new")}
        };
    }
}
