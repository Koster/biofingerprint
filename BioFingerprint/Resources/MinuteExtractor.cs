﻿using BioFingerprint.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BioFingerprint.Resources
{
    public static class MinuteExtractor
    {
        private static int width;
        private static int height;
        private static int[,] pixelsArray;
        private static FingerprintImages fingerprintImages;


        public static List<Minute> GetMinutesFromThinnedImage(Bitmap image, FingerprintImages fingerprintImagesObject)
        {
            width = image.Width;
            height = image.Height;
            pixelsArray = new int[image.Width, image.Height];
            pixelsArray = Helpers.BitmapToArray(image, pixelsArray, width, height);
            fingerprintImages = fingerprintImagesObject;

            List<Minute> result = new List<Minute>();

            // offset of 3, because counting angle requires 7x7 block
            for (int i = 3; i < width - 3; i++)
            {
                for (int j = 3; j < height - 3; j++)
                {
                    int cn = 0;
                    int angle = 0;
                    // if pixel is black
                    if (pixelsArray[i, j] == 1)
                    {
                        Minute newMinute = new Minute();
                        cn = GetCN(i, j);
                        // isolated point, do nothing
                        if (cn == 0) { }
                        // continuing ridge point, do nothing
                        if (cn == 2) { }
                        // crossing point, do nothing
                        if (cn == 4) { }
                        // ridge end point
                        if (cn == 1 || cn== 5)
                        {
                            angle = GetAngle(i, j, Enums.Enums.MinutiaeTypeEnum.EndPoint);
                            if (angle > 0 || 1==1)
                            {
                                newMinute.MinuteType = Enums.Enums.MinutiaeTypeEnum.EndPoint;
                                newMinute.Angle = angle;
                                newMinute.PixelPoint = new Point(i, j);
                                result.Add(newMinute);
                            }
                        }
                        // birurcation point
                        if (cn == 3)
                        {
                            angle = GetAngle(i, j, Enums.Enums.MinutiaeTypeEnum.BifurcationPoint);
                            if (angle > 0)
                            {
                                newMinute.MinuteType = Enums.Enums.MinutiaeTypeEnum.BifurcationPoint;
                                newMinute.Angle = angle;
                                newMinute.PixelPoint = new Point(i, j);
                                result.Add(newMinute);
                            }
                        }

                    }
                }
            }
            // PrintMinutes(result);

            ShowExtractedMinutiaOnCopyOfImage(pixelsArray, width, height, result);
            return result;
        }




        private static int GetCN(int x, int y)
        {
            int result = 0;
            //  Crossing Number Method
            // p1 - p2
            result += Math.Abs(pixelsArray[x + 1, y] - pixelsArray[x + 1, y - 1]);
            // p2 - p3
            result += Math.Abs(pixelsArray[x + 1, y - 1] - pixelsArray[x, y - 1]);
            // p3 - p4
            result += Math.Abs(pixelsArray[x, y - 1] - pixelsArray[x - 1, y - 1]);
            // p4 - p5
            result += Math.Abs(pixelsArray[x - 1, y - 1] - pixelsArray[x - 1, y]);
            // p5 - p6
            result += Math.Abs(pixelsArray[x - 1, y] - pixelsArray[x - 1, y + 1]);
            // p6 - p7
            result += Math.Abs(pixelsArray[x - 1, y + 1] - pixelsArray[x, y + 1]);
            // p7 - p8
            result += Math.Abs(pixelsArray[x, y + 1] - pixelsArray[x + 1, y + 1]);
            // p8 - p1 (because p9 = p1)
            result += Math.Abs(pixelsArray[x + 1, y + 1] - pixelsArray[x + 1, y]);

            int result2 = result / 2;
            //Console.WriteLine("CN: " + +result + " " + result2);
            
            /*
             //  Simple method
             // p1
             result += pixelsArray[x + 1, y];
             // p2
             result += pixelsArray[x + 1, y - 1];
             // p3
             result += pixelsArray[x, y - 1];
             // p4
             result += pixelsArray[x - 1, y - 1];
             // p5
             result += pixelsArray[x - 1, y];
             // p6 
             result += pixelsArray[x - 1, y + 1];
             // p7 
             result += pixelsArray[x, y + 1];
             // p8 
             result += pixelsArray[x + 1, y + 1];
             int result2 = result; */




            return result2;
        }


        private static int GetAngle(int x, int y, Enums.Enums.MinutiaeTypeEnum minuteType)
        {
            int result = 0;
            List<int> directionsList = new List<int>();
            if (pixelsArray[x + 3, y] == 1)
                directionsList.Add(1);
            if (pixelsArray[x + 3, y - 1] == 1)
                directionsList.Add(2);
            if (pixelsArray[x + 3, y - 2] == 1)
                directionsList.Add(3);
            if (pixelsArray[x + 3, y - 3] == 1)
                directionsList.Add(4);
            if (pixelsArray[x + 3, y - 3] == 1)
                directionsList.Add(4);
            if (pixelsArray[x + 2, y - 3] == 1)
                directionsList.Add(5);
            if (pixelsArray[x + 1, y - 3] == 1)
                directionsList.Add(6);
            if (pixelsArray[x, y - 3] == 1)
                directionsList.Add(7);
            if (pixelsArray[x - 1, y - 3] == 1)
                directionsList.Add(8);
            if (pixelsArray[x - 2, y - 3] == 1)
                directionsList.Add(9);
            if (pixelsArray[x - 3, y - 3] == 1)
                directionsList.Add(10);
            if (pixelsArray[x - 3, y - 2] == 1)
                directionsList.Add(11);
            if (pixelsArray[x - 3, y - 1] == 1)
                directionsList.Add(11);
            if (pixelsArray[x - 3, y] == 1)
                directionsList.Add(13);
            if (pixelsArray[x - 3, y + 1] == 1)
                directionsList.Add(14);
            if (pixelsArray[x - 3, y + 2] == 1)
                directionsList.Add(15);
            if (pixelsArray[x - 3, y + 3] == 1)
                directionsList.Add(16);
            if (pixelsArray[x - 2, y + 3] == 1)
                directionsList.Add(17);
            if (pixelsArray[x - 1, y + 3] == 1)
                directionsList.Add(18);
            if (pixelsArray[x, y + 3] == 1)
                directionsList.Add(19);
            if (pixelsArray[x + 1, y + 3] == 1)
                directionsList.Add(20);
            if (pixelsArray[x + 2, y + 3] == 1)
                directionsList.Add(21);
            if (pixelsArray[x + 3, y + 3] == 1)
                directionsList.Add(22);


            // calculate angle for end point
            if (minuteType == Enums.Enums.MinutiaeTypeEnum.BifurcationPoint && directionsList.Count >= 3)
            {
                int between12 = Math.Abs(directionsList[0] - directionsList[1]);
                int between23 = Math.Abs(directionsList[1] - directionsList[2]);
                int between13 = Math.Abs(directionsList[0] - directionsList[2]);

                // find minimal distance between lines
                if (between12 <= between23 && between12 <= between13)
                {
                    result = directionsList[0] + (between12 / 2);
                }
                else if (between23 <= between12 && between23 <= between13)
                {
                    result = directionsList[0] + (between23 / 2);
                }
                else if (between13 <= between23 && between13 <= between12)
                {
                    result = directionsList[0] + (between13 / 2);
                }
            }
            // calculate angle for birurcation
            else if (minuteType == Enums.Enums.MinutiaeTypeEnum.EndPoint && directionsList.Count >= 1)
            {
                result = directionsList[0];
            }

            else
            {
                return 0;
                throw new Exception("Wrong minute type in MinuteExtractor -> GetAngle");
            }

            // post process result
            result = (result - 1) * 15;
            return result;
        }


        #region helpers
        private static void PrintMinutes(List<Minute> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine("x: " + item.PixelPoint.X + ", y: " + item.PixelPoint.Y + ", minuteType: " + item.MinuteType + ", angle: " + item.Angle);
            }
        }
        #endregion



        public static void ShowExtractedMinutiaOnCopyOfImage(int[,] array, int width, int height, List<Minute> minuteList)
        {
            //BitmapData data = bmp.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
            Bitmap color_bmp = new Bitmap(width, height);
            bool showMinutesOnPrint = true;
            if (showMinutesOnPrint)
            {
                foreach (Minute minute in minuteList)
                {
                    int x = minute.PixelPoint.X;
                    int y = minute.PixelPoint.Y;
                    array[x + 3, y] = 2;
                    array[x + 3, y - 1] = 2;
                    array[x + 3, y - 2] = 2;
                    array[x + 3, y - 3] = 2;
                    array[x + 2, y - 3] = 2;
                    array[x + 1, y - 3] = 2;
                    array[x, y - 3] = 2;
                    array[x - 1, y - 3] = 2;
                    array[x - 2, y - 3] = 2;
                    array[x - 3, y - 3] = 2;
                    array[x - 3, y - 2] = 2;
                    array[x - 3, y - 1] = 2;
                    array[x - 3, y] = 2;
                    array[x - 3, y + 1] = 2;
                    array[x - 3, y + 2] = 2;
                    array[x - 3, y + 3] = 2;
                    array[x - 2, y + 3] = 2;
                    array[x - 1, y + 3] = 2;
                    array[x, y + 3] = 2;
                    array[x + 1, y + 3] = 2;
                    array[x + 2, y + 3] = 2;
                    array[x + 3, y + 3] = 2;
                    array[x + 3, y + 2] = 2;
                    array[x + 3, y + 1] = 2;
                }
            }
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {      
                        if (array[i, j] == 0)
                            color_bmp.SetPixel(i, j, Color.White);
                        else if (array[i, j] == 1)
                            color_bmp.SetPixel(i, j, Color.Black);
                        else if (array[i, j] == 2)
                            color_bmp.SetPixel(i, j, Color.Red);
                        else
                            color_bmp.SetPixel(i, j, Color.Purple);              
                }

            fingerprintImages.minutiaFingerprint = color_bmp;
        }


    }
}
