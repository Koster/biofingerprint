﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioFingerprint.Resources
{
    public static class URIs
    {
        public static Dictionary<string, string> UriDictionay = new Dictionary<string, string>()
        {
          {"ratePlotUri", "Resources/background_axis.png"}
        };
    }
}
