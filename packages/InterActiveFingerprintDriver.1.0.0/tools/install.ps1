param($installPath, $toolsPath, $package, $project)

regsvr32 Join-Path $toolsPath '\zkemkeeper.dll' /s

$project.Object.References | Where-Object { $_.Name -eq "zkemkeeperlib" } |  ForEach-Object { $_.EmbedInteropTypes = $false }